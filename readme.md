# CryoEM Common

Some commands and functions useful to work with cryo-electron
microscopy data.

Some projects using this:

- https://gitlab.com/jordibc/selecting-frequencies
- https://gitlab.com/jordibc/fourier-shell-correlation
- https://gitlab.com/jordibc/guinier-plot
- https://gitlab.com/jordibc/em-noise
- https://gitlab.com/jordibc/loc-b-factor
- https://gitlab.com/jordibc/local-snr
- https://gitlab.com/jordibc/local-resolution


## Dependencies

To use all the functionality of this notebook, you need to have the
modules `ipympl` for embedded matplotlib graphics, and `mrcfile` to
read 3D volume data (`pip install ipympl mrcfile`).
