{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0d8f807c",
   "metadata": {},
   "source": [
    "# Common definitions for cryoem notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d25245ee",
   "metadata": {},
   "source": [
    "This notebook contains useful commands and functions to work with cryoem data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1cb39f9",
   "metadata": {},
   "source": [
    "It can be \"imported\" by running `%run <path_to_file>/cryoem_common.ipynb` in a notebook. So, a new notebook would normally start with:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5e5a0b1",
   "metadata": {},
   "source": [
    "```py\n",
    "%run cryoem_common.ipynb\n",
    "add_latex_commands()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "759a7712",
   "metadata": {},
   "source": [
    "And then typically do things like:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca42ca67",
   "metadata": {},
   "source": [
    "```python\n",
    "# Read volume.\n",
    "V, voxel_n, voxel_size = get_vol_and_voxel('emd_10418_half_map_1.map')\n",
    "plot(V, title='Original volume V')\n",
    "\n",
    "# Precompute its Fourier transform.\n",
    "FV = fftn(V)\n",
    "fplot(abs(FV), slices=(90, 120, 190), title='$|\\mathcal{F}\\{V\\}|$')\n",
    "\n",
    "# Get ready to select frequencies (using a shell in frequency space).\n",
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)\n",
    "\n",
    "f_voxel_width = 4.6  # width (in voxels) of the shell\n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width\n",
    "\n",
    "def shell(f):\n",
    "    \"Return a shell of spatial frequencies, around frequency f\"\n",
    "    return exp(- (f_norm - f)**2 / (2 * f_width**2))\n",
    "\n",
    "# Define the spiral filter in frequency space.\n",
    "Hx, Hy, Hz = spiral_filter(voxel_n, voxel_size)\n",
    "\n",
    "# Create a map of amplitudes of the associated monogenic signal for a given frequency.\n",
    "f = 1/5  # in Angstroms^-1\n",
    "S = shell(f)\n",
    "\n",
    "SFV = S * FV  # volume at the frequency f (in frequency space)\n",
    "fplot(abs(SFV), title='$|S(f) \\mathcal{F}\\{V\\}|$ for frequency $f = %g \\, \\AA^{-1}$' % f)\n",
    "\n",
    "v0 = real(ifftn(SFV))  # volume \"at frequency f\"\n",
    "plot(v0, title='Volume at frequency $f = %g \\, \\AA^{-1}$' % f)\n",
    "\n",
    "vx = real(ifftn(Hx * SFV))\n",
    "vy = real(ifftn(Hy * SFV))\n",
    "vz = real(ifftn(Hz * SFV))\n",
    "\n",
    "amplitude = sqrt(v0**2 + vx**2 + vy**2 + vz**2)\n",
    "plot(amplitude, title='Amplitude map (for volume at frequency $f = %g \\, \\AA^{-1}$)' % f)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wound-galaxy",
   "metadata": {},
   "source": [
    "The functions that appear in the code above are defined and explained later in this notebook. And there are more commands and functions that could be useful in a typical cryoem analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7d8fd49",
   "metadata": {},
   "source": [
    "## Formulas in markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a42b5ac1",
   "metadata": {},
   "source": [
    "This are the latex-like commands to use in markdown for formulas. They are in a function because it needs to be explicitly called in the notebook that is going to use them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adult-villa",
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Markdown"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "lined-virtue",
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_latex_commands():\n",
    "    return Markdown(r\"\"\"\n",
    "Adding commands: `vect`, `norm`, `bra`, `ket`, `F`...\n",
    "$$\n",
    "\\newcommand{\\bra}[1]{\\left< #1 \\right|}\n",
    "\\newcommand{\\ket}[1]{\\left| #1 \\right>}\n",
    "\\newcommand{\\dot}[2]{\\left< #1 \\, \\middle| \\, #2 \\right>}\n",
    "\\newcommand{\\norm}[1]{\\left\\lVert #1 \\right\\rVert}\n",
    "\\newcommand{\\braf}[1]{\\left< #1 \\right|_f}\n",
    "\\newcommand{\\ketf}[1]{\\left| #1 \\right>_f}\n",
    "\\newcommand{\\dotf}[2]{\\left< #1 \\, \\middle| \\, #2 \\right>_f}\n",
    "\\newcommand{\\normf}[1]{\\left\\lVert #1 \\right\\rVert_f}\n",
    "\\newcommand{\\vect}[1]{\\boldsymbol{#1}}\n",
    "\\newcommand{\\F}[1]{\\mathcal{F} \\! \\left\\{ #1 \\right\\}}\n",
    "\\newcommand{\\IF}[1]{\\mathcal{F}^{-1} \\!\\! \\left\\{ #1 \\right\\}}\n",
    "\\newcommand{\\E}[1]{\\mathop{\\mathbb{E} \\! \\left[ #1 \\right]}}\n",
    "\\newcommand{\\FSC}{\\text{FSC}}\n",
    "\\newcommand{\\SNR}{\\text{SNR}}\n",
    "$$\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "pending-present",
   "metadata": {},
   "source": [
    "## Imports"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89c4de39",
   "metadata": {},
   "source": [
    "To make embedded plots in the notebook, we use matplotlib in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "convenient-portrait",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1d3af1f",
   "metadata": {},
   "source": [
    "We want to use many common mathematical functions from numpy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "white-emerald",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from numpy import (\n",
    "    arange, r_, linspace, meshgrid, zeros, ones, eye, nan_to_num,\n",
    "    real, imag, conjugate, logical_and, quantile, floor,\n",
    "    max, min, abs, sum, sqrt, exp, log, cos, sin, tan, sinc,\n",
    "    arccos, arcsin, arctan, arctan2)\n",
    "\n",
    "from numpy.fft import fft, ifft, fftn, ifftn, fftshift, ifftshift, fftfreq"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41d6a628",
   "metadata": {},
   "source": [
    "And we often want to do things in parallel. We can use the pool from `multiprocessing`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41427842",
   "metadata": {},
   "outputs": [],
   "source": [
    "from multiprocessing import Pool"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5206f127",
   "metadata": {},
   "source": [
    "which will typically be used as:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05721253",
   "metadata": {},
   "source": [
    "```py\n",
    "%%time\n",
    "with Pool() as pool:\n",
    "    ys = pool.map(f, xs)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54543b5f",
   "metadata": {},
   "source": [
    "We use the [`mrcfile`](https://pypi.org/project/mrcfile/) module to read and write structural biology volume data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "encouraging-credit",
   "metadata": {},
   "outputs": [],
   "source": [
    "import mrcfile"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c568de1",
   "metadata": {},
   "source": [
    "Finally, if it is available, we use [`ipyvolume`](https://ipyvolume.readthedocs.io/en/latest/) to make a 3D plot of volumes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9efce9e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    import ipyvolume\n",
    "    def volplot(a):\n",
    "        return ipyvolume.quickvolshow(a)\n",
    "except:\n",
    "    def volplot(a):\n",
    "        print('Not showing 3D representation of volume: ipyvolume not available.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "alternate-verse",
   "metadata": {},
   "source": [
    "## Reading volumes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4ee7b51",
   "metadata": {},
   "source": [
    "The volumes we are going to consider are all defined on a grid of size $n \\times n \\times n$, where the voxel size is the same in every direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "consistent-politics",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_vol_and_voxel(fname):\n",
    "    \"Return the volume and the voxel number and size corresponding to the map in file fname\"\n",
    "    mrc = mrcfile.open(fname)\n",
    "    \n",
    "    nx, ny, nz = mrc.data.shape\n",
    "    assert nx == ny == nz\n",
    "    \n",
    "    vs = mrc.voxel_size\n",
    "    assert vs.x == vs.y == vs.z\n",
    "    \n",
    "    return mrc.data, nx, vs.x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14e58776",
   "metadata": {},
   "source": [
    "## Masks"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8841d48f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def mask_in_sphere(n):\n",
    "    \"Return a mask that selects the voxels of a n*n*n grid within the contained sphere\"\n",
    "    coords = np.r_[:n] - n/2\n",
    "    x, y, z = meshgrid(coords, coords, coords)\n",
    "    r = sqrt(x**2 + y**2 + z**2)\n",
    "    return r < n/2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "varying-friendly",
   "metadata": {},
   "source": [
    "## Frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a62a469",
   "metadata": {},
   "source": [
    "The convention I follow is to have the *positions* in a volume encoded as given by `meshgrid` (so, from $-n$ to $n$ in each dimension), and the *frequencies* encoded as `fftfreq` would return (so, the 0 frequency appears in the first bin, etc., and thus they are [ready to be ifft-ed](https://stackoverflow.com/questions/42151936/correct-order-of-implementing-fftshift-and-ifftshift-in-python))."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "256271ff",
   "metadata": {},
   "source": [
    "This way the change between frequency and space domains are always like `F = fft(X)` and `X = ifft(F)`. And we only use `fftshift` when plotting, and nothing more."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "figured-anatomy",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fftnfreq(n, d=1):\n",
    "    \"Return the Discrete Fourier Transform sample frequencies\"\n",
    "    f = fftfreq(n, d)\n",
    "    return meshgrid(f, f, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5da41efd",
   "metadata": {},
   "source": [
    "### Shell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01695bcb",
   "metadata": {},
   "source": [
    "A \"*shell at frequency* $f$\" is a normalized smooth approximation to a delta at that frequency."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "killing-criterion",
   "metadata": {},
   "source": [
    "There are several ways to \"select a frequency\" (or \"use a band-pass\" filter), each with different trade-offs. There is an interesting discussion on the choice of band-pass quadrature filters in [Boukerroui et al.](http://link.springer.com/10.1023/B:JMIV.0000026557.50965.09) for example."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2204daeb",
   "metadata": {},
   "source": [
    "Since creating a shell of frequencies often happens inside a loop, we don't create the function here. We will assume that we have precomputed `f_norm` and `f_width`, so each call to `shell(f)` doesn't have to recompute them. The function then will be defined in the notebook like:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f4cee63",
   "metadata": {},
   "source": [
    "```py\n",
    "def shell(f):\n",
    "    \"Return a shell of spatial frequencies, around frequency f\"\n",
    "    return exp(- (f_norm - f)**2 / (2 * f_width**2))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4e4cf7b",
   "metadata": {},
   "source": [
    "where the values of `f_norm` (which is of type $n \\times n \\times n$) and `f_width` (a scalar) will be chosen according to the volume studied."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "heavy-navigation",
   "metadata": {},
   "source": [
    "The way to create `f_norm` based on `voxel_n` and `voxel_size` is:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "terminal-kennedy",
   "metadata": {},
   "source": [
    "```python\n",
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "chief-europe",
   "metadata": {},
   "source": [
    "And a typical way to choose `f_width` is to make it cover a certain width in pixels in frequency space, with something like:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02a04774",
   "metadata": {},
   "source": [
    "```py\n",
    "f_voxel_width = 4.6  # width (in voxels) of the shell\n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fade5516",
   "metadata": {},
   "source": [
    "## Spiral filter"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "capital-miracle",
   "metadata": {},
   "source": [
    "We call \"spiral filter\" to what they call the \"quadrature transform\" in [Quiroga et al.](https://ui.adsabs.harvard.edu/abs/2003OptCo.224..221A/abstract) for example. This is different from the [\"quadrature filter\" as it appears in wikipedia](https://en.wikipedia.org/wiki/Quadrature_filter) -- the latter including the $\\delta(t)$ term in the time domain (same as a \"+1\" term in the frequency domain) that includes the original signal as the real part of the transformed one."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cognitive-danish",
   "metadata": {},
   "source": [
    "The result of applying to the original signal these \"spiral filters\" for the $x$, $y$, and $z$ coordinates, when properly added to the original signal, produces a [*monogenic signal*](https://en.wikipedia.org/wiki/Analytic_signal#The_monogenic_signal) (which is the multidimensional generalization of an *analytic signal*). A good description of what is going on and how it is useful appears in an [article by C. P. Bridge](http://arxiv.org/abs/1703.09199)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wireless-baking",
   "metadata": {},
   "source": [
    "The filters are defined here in the frequency domain (since it is easiest and also it is the way we will normally use them):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acb653eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "def spiral_filter(voxel_n, voxel_size):\n",
    "    \"Return the freq-domain spiral filter for the three dimensions (x, y, z)\"\n",
    "    fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "    f_norm = sqrt(fx**2 + fy**2 + fz**2)\n",
    "    \n",
    "    def H(fi):\n",
    "        return -1j * nan_to_num(fi / f_norm)  # nan_to_num -> set to 0 the 0/0 bin\n",
    "\n",
    "    with np.errstate(invalid='ignore'):  # ignore warning about dividing by 0 in one bin\n",
    "        return H(fx), H(fy), H(fz)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "common-tiffany",
   "metadata": {},
   "source": [
    "We normally will use it in the following way (where `SFV` is a frequency shell of the Fourier transform of the volume we are interested in, that is `shell(f) * fftn(V)`):"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ordinary-population",
   "metadata": {},
   "source": [
    "```py\n",
    "Hx, Hy, Hz = spiral_filter(voxel_n, voxel_size)\n",
    "\n",
    "vx = real(ifftn(Hx * SFV))\n",
    "vy = real(ifftn(Hy * SFV))\n",
    "vz = real(ifftn(Hz * SFV))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "silver-tiffany",
   "metadata": {},
   "source": [
    "Note that the `ifftn`s should be real (within numerical accuracy), since both `SFV` and the `Hi`s are functions with $f(-k) = f^*(k)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "described-pizza",
   "metadata": {},
   "source": [
    "And now we can for example get the amplitude, phase, and direction (with two angles in spherical coordinates) of the associated monogenic signal this way:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rural-patio",
   "metadata": {},
   "source": [
    "```py\n",
    "v0 = real(ifftn(SFV))  # volume \"at frequency f\"\n",
    "\n",
    "vxy = sqrt(vx**2 + vy**2)\n",
    "v1 = sqrt(vxy**2 + vz**2)  # amplitude of the quadrature signal\n",
    "\n",
    "amplitude = sqrt(v0**2 + v1**2)\n",
    "phase = arctan2(v1, v0)\n",
    "\n",
    "theta = arctan2(vxy, vz)  # angles of vector pointing in the\n",
    "phi = arctan2(vy, vx)     # direction of the phase change\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "numerical-story",
   "metadata": {},
   "source": [
    "## Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9b4b4a6",
   "metadata": {},
   "source": [
    "Nice plots with 3 slices of a volume, for the space and frequency (Fourier) domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "handmade-brick",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(a, title='', slices=None, add_colorbar=False, lims=None):\n",
    "    \"Plot slices of constant i, j, k of the volume a\"\n",
    "    i, j, k = slices or [ii//2 for ii in a.shape]  # use the center slices by default\n",
    "    ext = [lims[0], lims[1], lims[0], lims[1]] if lims else None\n",
    "    \n",
    "    plt.figure(figsize=(9, 3))\n",
    "    plt.suptitle(title)\n",
    "    \n",
    "    plt.subplot(131)\n",
    "    plt.imshow(a[i,:,:], extent=ext, origin='lower')\n",
    "    if not lims:\n",
    "        plt.axis('off')\n",
    "    plt.title(f'fixed i (slice {i})')\n",
    "\n",
    "    plt.subplot(132)\n",
    "    plt.imshow(a[:,j,:], extent=ext, origin='lower')\n",
    "    if not lims:\n",
    "        plt.axis('off')\n",
    "    plt.title(f'fixed j (slice {j})')\n",
    "\n",
    "    ax = plt.subplot(133)\n",
    "    im = plt.imshow(a[:,:,k], extent=ext, origin='lower');\n",
    "    if not lims:\n",
    "        plt.axis('off')\n",
    "    plt.title(f'fixed k (slice {k})')\n",
    "\n",
    "    if add_colorbar:\n",
    "        # This is a little bit nicer than just doing plt.colorbar()\n",
    "        from mpl_toolkits.axes_grid1 import make_axes_locatable\n",
    "        divider = make_axes_locatable(ax)\n",
    "        cax = divider.append_axes('right', size=0.05, pad=0.2)\n",
    "        plt.colorbar(im, cax=cax)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "digital-giving",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fplot(a, *args, **kwargs):\n",
    "    \"Plot frequency slices\"\n",
    "    plot(fftshift(a), *args, **kwargs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00d6bf32",
   "metadata": {},
   "source": [
    "## Interpolations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58fb9dbb",
   "metadata": {},
   "source": [
    "To quickly find at which point a function crosses a certain value, we can use the following functions: `bisect` uses the bisection algorithm to find the index of the array when the crossing happens, and `linear_interpolation` gives the approximate value of $x$ at that point."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21487129",
   "metadata": {},
   "source": [
    "They can be used for example to find the resolution of a volume by computing its Fourier Shell Correlation at different frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3a759e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def bisect(ys, y_goal):\n",
    "    \"Return the index just before the ys cross y_goal\"\n",
    "    def is_between(y0, y1):\n",
    "        return y0 <= y_goal < y1 or y0 >= y_goal > y1\n",
    "\n",
    "    i0, i1 = 0, len(ys) - 1  # indices at the extremes\n",
    "    assert is_between(ys[i0], ys[i1]), f'not between the array extremes: {y_goal}'\n",
    "\n",
    "    while i1 - i0 > 1:\n",
    "        i = (i0 + i1) // 2  # midpoint index\n",
    "        if is_between(ys[i0], ys[i]):  # y_goal in the first bisection\n",
    "            i1 = i  # move rightmost limit to the center\n",
    "        else:  # y_goal in the second bisection\n",
    "            i0 = i  # move leftmost limit to the center \n",
    "\n",
    "    return i0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94bf77d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def linear_interpolation(xs, ys, y_goal):\n",
    "    \"Return x_goal such that (x_goal, y_goal) interpolates the function sampled as zip(xs, ys)\"\n",
    "    i = bisect(ys, y_goal)\n",
    "    \n",
    "    x0, x1 = xs[i], xs[i+1]\n",
    "    y0, y1 = ys[i], ys[i+1]\n",
    "\n",
    "    dy_r = (y_goal - y0) / (y1 - y0)  # relative change in y\n",
    "    return x0 + dy_r * (x1 - x0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "private-multimedia",
   "metadata": {},
   "source": [
    "## Iteration over voxels"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "emotional-healing",
   "metadata": {},
   "source": [
    "If we cannot use matrix or tensor multiplications to compute the quantities of interest exploiting all the optimizations and parallelism of the lineal algebra libraries, we can still iterate over all the voxels and (more or less reasonably) compute interesting things in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "official-maryland",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_voxels(V):\n",
    "    \"Yield triplets (i,j,k) that travel thru all voxels of volume V\"\n",
    "    index_iterator = np.nditer(V, flags=['multi_index'])\n",
    "    for x in index_iterator:\n",
    "        yield index_iterator.multi_index"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "concerned-image",
   "metadata": {},
   "source": [
    "Which then we can for example use to get the map with results of applying function `func` to each voxel like:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "normal-checkout",
   "metadata": {},
   "source": [
    "```py\n",
    "%%time\n",
    "with Pool() as pool:\n",
    "    results_map = np.array(pool.map(func, get_voxels(V)))\n",
    "results_map.shape = V.shape  # reshape all voxels into a n*n*n volume\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
